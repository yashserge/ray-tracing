package config

// Camera describes a camera config.
type Camera struct {
	Position   Vector     `mapstructure:"position"`
	Direction  Vector     `mapstructure:"direction"`
	Resolution Resolution `mapstructure:"resolution"`
	Angle      float64    `mapstructure:"angle"` // radian
}

// Vector describes a 3d vector config.
type Vector struct {
	X float64 `mapstructure:"x"` // meter
	Y float64 `mapstructure:"y"` // meter
	Z float64 `mapstructure:"z"` // meter
}

// Resolution describes image resolution config.
type Resolution struct {
	Height int `mapstructure:"height"` // pixel
	Width  int `mapstructure:"width"`  // pixel
}
