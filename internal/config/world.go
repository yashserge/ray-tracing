package config

// World describes a world config.
type World struct {
	TimeUnit      float64 `mapstructure:"time_unit"`       // seconds
	LightVelocity float64 `mapstructure:"light_velocity"`  // m/s
	G             float64 `mapstructure:"g"`               // m3/kgs2
	EventHorizon  float64 `mapstructure:"event_horizon"`   // meter
	BlackHoleMass float64 `mapstructure:"black_hole_mass"` // kg
}
