package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

// Config is a basic config.
type Config struct {
	Camera Camera `mapstructure:"camera"`
	World  World  `mapstructure:"world"`
}

// New initializes a configuration.
func New() (*Config, error) {
	var config = &Config{}
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); nil != err {
		return nil, fmt.Errorf("unable to read config from file")
	}

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.Unmarshal(config)
	if nil != err {
		return nil, fmt.Errorf("unable to decode into struct: '%w'", err)
	}

	return config, nil
}
