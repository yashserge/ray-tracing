package model

// Sphere describes a sphere object such as ball, planet, black hole, star etc.
type Sphere struct {
	Object

	Center Vector
	Radius float64 // meter
	Angle  float64 // radian
	Mass   float64 // kg
}

// NewSphere is a Sphere constructor.
func NewSphere(center Vector, radius float64, angle float64, mass float64) *Sphere {
	return &Sphere{
		Center: center,
		Radius: radius,
		Angle:  angle,
		Mass:   mass,
	}
}
