package model

// Vector is a 3d vector.
type Vector struct {
	X, Y, Z float64
}
