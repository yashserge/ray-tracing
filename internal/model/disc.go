package model

// Disc describes planetary ring and black hole' accretion disk.
type Disc struct {
	Object

	Center Vector
	Radius float64 // meter
	Angle  float64 // radian
}

// NewDisc is a Disc constructor.
func NewDisc(center Vector, radius float64, angle float64) *Disc {
	return &Disc{
		Center: center,
		Radius: radius,
		Angle:  angle,
	}
}
