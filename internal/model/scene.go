package model

// Object is an interface that describes scene objects.
type Object interface{}

// Scene describes the scene consisting of all objects.
type Scene struct {
	Objects []Object
}

// NewScene is a Scene constructor.
func NewScene() *Scene {
	return &Scene{}
}

// AddObject adds an Object to the Scene.
func (s *Scene) AddObject(object Object) {
	s.Objects = append(s.Objects, object)
}

// AddObjects adds Objects to the Scene.
func (s *Scene) AddObjects(objects ...Object) {
	s.Objects = append(s.Objects, objects...)
}
