package model

// Camera describes a camera.
type Camera struct {
	Position   Vector
	Direction  Vector
	Resolution Resolution
	Angle      float64
}

// Resolution of the camera.
type Resolution struct {
	X float64
	Y float64
}
