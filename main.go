package main

import (
	"fmt"
	"log"

	"ray-tracing/internal/config"
	"ray-tracing/internal/model"
)

func main() {
	config, err := config.New()
	if err != nil {
		panic(err)
	}

	log.Default().Print("initialized config")
	fmt.Printf("%#v\n", config)

	_ = model.NewScene()
	// sphere := model.NewSphere()
	// scene.AddObject(sphere)
}
